Collection of docker image `Dockerfile`s

## Images

### Clang11
Hosted on [Docker Hub](https://hub.docker.com/repository/docker/lukapg/clang11)

Image based on Ubuntu 20.04. Contains Clang11, CMake 3.16 and vcpkg. Also installed through vcpkg is Catch2